extern crate image;
extern crate num_complex;

use num_complex::Complex;

pub fn plot() {
    // image dimensions
    let width: u32 = 800;
    let height: u32 = 600;

    let xmin: f64 = -2.0;
    let xmax: f64 = 1.0;

    let ymin: f64 = -1.5;
    let ymax: f64 = 1.5;

    let scalex = (xmax - xmin) / height as f64;
    let scaley = (ymax - ymin) / height as f64;

    // divergence
    let limit: f64 = 2.0;
    // convergence
    let mut max: isize = 1000;

    let color: bool = true;
    if color { max = 256; }

    // initialize image buffer
    let mut imgbuf = image::ImageBuffer::new(width, height);
    //let mut imgbuf = image::ImageBuffer::new(width, width);

    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let cx = xmin + x as f64 * scalex;
        let cy = ymin + y as f64 * scaley;

        let mut iterations: isize = 0;

        let c = Complex::new(cx, cy);
        let mut z = Complex::new(0f64, 0f64);

        for _ in 0..max {
            if z.norm() > limit { break; }
            z = z * z + c;
            iterations += 1;
        }

        *pixel = image::Luma([iterations as u8]);
        //if color {
        //    *pixel = image::Luma([iterations as u8]);
        //} else {
        //    if iterations < max {
        //        *pixel = image::Rgb([255, 255, 255]); //white
        //    } else {
        //        *pixel = image::Rgb([0, 0, 0]); //black
        //    }
        //}
        //let r: u8 = (iterations as u8 >> 5) * 255 / 7;
        //let g: u8 = ((iterations as u8 >> 2) & 0x07) * 255 / 7;
        //let b: u8 = (iterations as u8 & 0x03) * 255 / 3;
        //*pixel = image::Rgb([r, g, b]);
        //let r: u8 = iterations as u8 / 255;
        //let g: u8 = 1;
        //let b: u8 = iterations as u8 / (iterations as u8 + 8);
        //*pixel = image::Rgb([r, g, b]);
    }

    imgbuf.save("fractal.png").unwrap();
}
