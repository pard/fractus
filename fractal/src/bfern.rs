use rand::prelude::*;

pub fn plot(resolution: usize) -> Vec<Vec<f64>> {
    let mut array = vec![vec![0.0,0.0]];
    for i in 0..resolution {
        let x = array[i][0];
        let y = array[i][1];
        let probability: f64 = random();
        if probability < 0.01 {
            array.push(
                vec![(0.0 * x), (0.16 * y)]);
        } else if probability > 0.01 && probability < 0.85 {
            array.push(
                vec![(0.85 * x + 0.04 * y), (-0.04 * x + 0.85 * y + 1.60)]);
        } else if probability > 0.05 && probability < 0.92 {
            array.push(
                vec![(0.20 * x - 0.26 * y), (0.23 * x + 0.22 * y + 1.60)]);
        } else {
            array.push(
                vec![(-0.15 * x + 0.28 * y), (0.26 * x + 0.24 * y + 0.44)]);
        }
    }
    array
}
